from bottle import route, run
import os

@route('/')
def hello():
    return "Hello World!"


run(host='0.0.0.0' , port=8080, debug=True)
